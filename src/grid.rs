use image::DynamicImage;
use log::debug;

#[derive(Debug)]
pub struct Grid {
    inner: Vec<bool>,
    inner_temp: Vec<bool>,
    pub width: usize,
    pub height: usize,
}

impl Grid {
    pub fn new(img: DynamicImage) -> Grid {
        let img = img.into_rgb8();
        let height = img.height() as usize;
        let width = img.width() as usize;
        debug!("{} {}", width, height);
        let mut g = Grid {
            inner: Vec::with_capacity(width * height),
            width,
            height,
            inner_temp: Vec::with_capacity(width * height),
        };
        let pixels = img.pixels();
        for p in pixels {
            g.inner.push(p[0] != 255);
        }

        g.inner_temp = g.inner.clone();
        g
    }

    pub fn update(&mut self) {
        for i in 0..self.inner.len() {
            self.calc(i);
        }
        self.inner = self.inner_temp.clone();
    }

    fn coord_to_index(&self, coord: (usize, usize)) -> usize {
        coord.0 + (coord.1 * self.width)
    }

    fn index_to_coord(&self, index: usize) -> (usize, usize) {
        let x = index % self.width;
        let y = index / self.width;
        (x, y)
    }

    fn calc(&mut self, index: usize) {
        let (x, y) = self.index_to_coord(index);
        let alive_neighbors: Vec<Option<bool>> = self
            .get_neighbors(x, y)
            .into_iter()
            .filter(|node| node.is_some())
            .filter(|node| node.unwrap())
            .collect();
        if self.inner_temp[index] {
            match alive_neighbors.len() {
                2 | 3 => (),
                _ => self.inner_temp[index] = false,
            }
        } else if alive_neighbors.len() == 3 { self.inner_temp[index] = true }
    }

    fn get_neighbors(&self, x: usize, y: usize) -> Vec<Option<bool>> {
        let left = if x == 0 {
            None
        } else {
            Some(self.inner[self.coord_to_index((x - 1, y))])
        };

        let right = if x == (self.width - 1) {
            None
        } else {
            Some(self.inner[self.coord_to_index((x + 1, y))])
        };

        let mut top3 = if y == 0 {
            vec![None; 8]
        } else {
            let mut v = Vec::with_capacity(8);
            v.push(Some(self.inner[self.coord_to_index((x, y - 1))]));

            if x == 0 {
                v.push(None);
            } else {
                v.push(Some(self.inner[self.coord_to_index((x - 1, y - 1))]));
            }

            if x == (self.width - 1) {
                v.push(None);
            } else {
                v.push(Some(self.inner[self.coord_to_index((x + 1, y - 1))]));
            }

            v
        };

        let bottom3 = if y == (self.height - 1) {
            vec![None; 3]
        } else {
            let mut v = Vec::with_capacity(3);
            v.push(Some(self.inner[self.coord_to_index((x, y + 1))]));

            if x == 0 {
                v.push(None);
            } else {
                v.push(Some(self.inner[self.coord_to_index((x - 1, y + 1))]));
            }

            if x == (self.width - 1) {
                v.push(None);
            } else {
                v.push(Some(self.inner[self.coord_to_index((x + 1, y + 1))]));
            }

            v
        };

        top3.extend(bottom3);
        top3.push(left);
        top3.push(right);

        top3
    }

    pub fn draw(&self, screen: &mut [u8]) {
        debug_assert_eq!(screen.len(), 4 * self.inner.len());
        for (c, pix) in self.inner.iter().zip(screen.chunks_exact_mut(4)) {
            let color = if *c {
                [0, 0xff, 0xff, 0xff]
            } else {
                [0, 0, 255, 0xff]
            };
            pix.copy_from_slice(&color);
        }
    }
}
